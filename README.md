# DRAGONS AHEAD

unfinished, will probably stay this way, but!, you still can use the CooreDNS binary that fell out of CI (Added external redis plugin)
```bash
curl -SL "https://gitlab.com/Underknowledge/coredns-redis/-/jobs/artifacts/main/download?job=build" -o /tmp/coredns-redis.go.zip
mkdir -p /tmp/artifacts
unzip -j /tmp/coredns-redis.go.zip -d /tmp/artifacts
pushd /tmp/
mv /tmp/artifacts/coredns /usr/bin/coredns && rm -rf /tmp/artifacts
popd
restorecon -v /usr/bin/coredns
```



It was fun as long as it lasted...     
The idea was to have an reliable DNS setup with little overhead. 

redis seemd nice as it is trivial to setup a cluster or secondary stores. 
but I guess the redis plugin let me down. 


```
[root@CoreDNS03 ~ ] took 5s ❯ dig +short SOA @10.0.0.100 arpa.corp 
;; Got bad packet: bad label type
112 bytes
84 38 85 00 00 01 00 01 00 00 00 01 04 61 72 70          .8...........arp
61 04 63 6f 72 70 00 00 06 00 01 04 61 72 70 61          a.corp......arpa
04 63 6f 72 70 00 00 06 00 01 00 00 00 64 00 29          .corp........d.)
09 6e 73 31 57 68 6f 61 6d 49 04 61 72 70 61 04          .ns1WhoamI.arpa.
63 6f 72 70 00 65 1b 1a 88 00 00 00 2c 00 00 00          corp.e......,...
37 00 00 00 43 00 00 00 64 00 00 29 04 d0 00 00          7...C...d..)....
00 00 00 0c 00 0a 00 08 e1 de cf 80 b8 97 8d d4          ................
```
also CNAMES do not resolve to addresses and I can not get authoritative awnsers back