



# # REMEMBER, it is prittey sure faster do download a new image then to upgrade over 3 major versions... 
# sudo dnf upgrade --refresh && sudo dnf install dnf-plugin-system-upgrade -y && \
# sudo dnf system-upgrade download --releasever=37
# # reboot
# sudo dnf install rpmconf -y
# sudo rpmconf -a
# sudo dnf system-upgrade download --releasever=38

dnf install -y qemu-guest-agent
systemctl enable --now qemu-guest-agent

if grep -q '^DNSStubListener=' /etc/systemd/resolved.conf; then
    # If it does, change its value
    sed -i 's/^DNSStubListener=.*/DNSStubListener=no/' /etc/systemd/resolved.conf
else
    # Otherwise, append it at the end of the file
    echo "DNSStubListener=no" >> /etc/systemd/resolved.conf
fi
sudo systemctl restart systemd-resolved

curl -fsSL https://starship.rs/install.sh -o /tmp/starship-installer.sh
sh /tmp/starship-installer.sh -f --bin-dir="/usr/bin"
cat << 'EOHELPER' > /etc/profile.d/starship.sh 
export STARSHIP_CONFIG=/etc/starship.toml
eval "$(starship init bash)"
EOHELPER
cat << 'EOTOML' > /etc/starship.toml
# Inserts a blank line between shell prompts
add_newline = false 
[username]
format = "[\\[](bold)[$user]($style)@"
show_always = true
[hostname]
# https://misc.flogisoft.com/_media/bash/colors_format/256_colors_fg.png
format =  "[$hostname](bold bright-blue)"
ssh_only = false
disabled = false
[directory]
format =  " [$path]($style)[$read_only]($read_only_style) [\\]](bold) "
[line_break]
disabled = true
[memory_usage]
disabled = false
threshold = 80 
style = "dimmed green"
# Disable the package module, hiding it from the prompt completely
[package]
disabled = true
EOTOML
sed -i "s/bold bright-blue/bold fg:177/" /etc/starship.toml

LATEST_VERSION=$(curl -s "https://api.github.com/repos/coredns/coredns/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
echo "Installing CoreDNS version $LATEST_VERSION..."
curl -L -O "https://github.com/coredns/coredns/releases/download/${LATEST_VERSION}/coredns_${LATEST_VERSION:1}_linux_amd64.tgz"
tar -xvzf "coredns_${LATEST_VERSION:1}_linux_amd64.tgz"
mv coredns /usr/local/bin/
chmod +x /usr/bin/coredns
restorecon -v /usr/bin/coredns
mkdir -p /etc/coredns/


curl -SL "https://gitlab.com/Underknowledge/coredns-redis/-/jobs/artifacts/main/download?job=build" -o /tmp/coredns-redis.go.zip
mkdir -p /tmp/artifacts
unzip -j /tmp/coredns-redis.go.zip -d /tmp/artifacts
pushd /tmp/
mv /tmp/artifacts/coredns /usr/bin/coredns && rm -rf /tmp/artifacts
popd
restorecon -v /usr/bin/coredns



cat <<EOF > /etc/coredns/Corefile
. {
    forward . 10.0.0.27 tls://9.9.9.9 {
        except arpa.corp
    }
    health :8080
    prometheus :9153
    cache
    errors
    log
}

arpa.corp {
    $(if [[ $(hostname) == *"01"* ]]; then
        cat <<EOSUB
file /etc/coredns/arpa.corp.db
    acl {
      # allow type AXFR net 10.0.0.0/22
      # allow type IXFR net 10.0.0.0/22
      allow type AXFR net 10.0.0.101/32
      allow type IXFR net 10.0.0.101/32
      block type AXFR net *
      block type IXFR net *
    }
    transfer {
        to 10.0.0.101
    }
EOSUB
      else
        cat <<EOSUB
secondary {
        transfer from 10.0.0.100
    }
EOSUB
      fi)
    log
}
EOF


# Create the domain database file
cat <<'EOF' > /etc/coredns/arpa.corp.db
;
; Zone file for arpa.corp
;
$TTL 604800
@       IN      SOA     ns.arpa.corp. admin.arpa.corp. (
                        3       ; Serial
                        604800  ; Refresh
                        86400   ; Retry
                        2419200 ; Expire
                        604800  ; Negative Cache TTL
                        )
; Name servers
@       IN      NS      ns.arpa.corp.

; A records for the domain
ns1     IN      A       10.0.0.100
ns2     IN      A       10.0.0.101
proxmox IN      A       10.0.0.210
test    IN      A       10.0.0.123
EOF


# /usr/bin/coredns -conf /etc/coredns/Corefile 
#  dig @127.0.0.1 ns.arpa.corp
#  dig @127.0.0.1 ns.arpa.corp
#  dig @10.0.0.101 dnsup.arpa.corp


getent group coredns >/dev/null || groupadd -r coredns
getent passwd coredns >/dev/null || \
  useradd -r -g coredns -s /sbin/nologin \
          -c "CoreDNS services" coredns

# sudo setcap 'cap_net_bind_service=+ep' /usr/bin/coredns

cat <<'EOF' > /etc/systemd/system/coredns.service
[Unit]
After=network.target
Description=coredns server
Documentation=http://coredns.io

[Service]
User=coredns
Type=simple
EnvironmentFile=/etc/sysconfig/coredns
ExecStart=/usr/bin/coredns --conf=/etc/coredns/Corefile $EXTRA_ARGS
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure
RestartSec=10
SuccessExitStatus=0

[Install]
WantedBy=multi-user.target
EOF


mkdir -p /etc/systemd/system/coredns.service.d/
cat <<'EOF' > /etc/systemd/system/coredns.service.d/override.conf
[Service]
AmbientCapabilities=CAP_NET_BIND_SERVICE
CapabilityBoundingSet=CAP_NET_BIND_SERVICE
NoNewPrivileges=true
EOF
systemctl daemon-reload

echo 'EXTRA_ARGS=' > /etc/sysconfig/coredns 
touch /var/log/coredns.log /var/log/coredns.log
cat <<'EOF' > /etc/logrotate.d/coredns
/var/log/coredns.log {
  rotate 5
  size 10M
  compress
  notifempty
}

/var/log/coredns.err.log {
  rotate 5
  size 10M
  compress
  notifempty
}
EOF
logrotate --debug /etc/logrotate.d/coredns
echo 'd /var/lib/coredns 0755 coredns coredns -' > /etc/tmpfiles.d/coredns.conf
systemd-tmpfiles --create coredns.conf
systemctl daemon-reload
systemctl enable --now coredns.service
systemctl status coredns.service
# journalctl -u coredns.service -f
firewall-cmd --permanent --add-port=53/udp --add-port=53/tcp --add-port=8080/tcp


touch /etc/coredns/arpa.corp.db /var/log/coredns.err.log /var/log/coredns.log
chown coredns:coredns /etc/coredns/arpa.corp.db /var/log/coredns.err.log /var/log/coredns.log

# Update Nameserver Records at the Registrar:
#   Log in to the control panel of your domain registrar.
#   Update the nameserver (NS) records for "Anotherexampledomain.cc" to point to the public IP addresses of your DNS servers.




# implement
#  view 
#  tls
#  sign? (useful with secondary?)
#  Prometheut (and firewall)
#  geoip for fun


# we just install redis to 
dnf install redis 'dnf-command(versionlock)' -y
# dnf versionlock add coredns
mv /etc/redis/redis.conf /etc/redis/redis.orig
# cp /etc/redis/redis.orig /etc/redis/redis.conf


# replicaof <master-ip> <master-port>


sudo systemctl restart redis
echo "cluster-enabled yes" >> /etc/redis/redis.conf
echo "cluster-config-file nodes.conf" >> /etc/redis/redis.conf
echo "cluster-node-timeout 5000" >> /etc/redis/redis.conf
echo "appendonly yes" >> /etc/redis/redis.conf
echo "requirepass MyPasswoRdSucks" >> /etc/redis/redis.conf

redis-cli -a MyPasswoRdSucks --cluster create 10.0.0.100:6379 10.0.0.101:6379 --cluster-replicas 0


systemctl start redis.service
systemctl status redis.service