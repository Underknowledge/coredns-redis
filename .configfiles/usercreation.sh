preferred_id=999
while true; do
    if ! getent group $preferred_id >/dev/null && ! getent passwd $preferred_id >/dev/null; then
        # Both UID and GID are available
        break
    fi
    ((preferred_id -- ))
    # safeguard to prevent infinite loops or going to undesirable low IDs
    if [[ $preferred_id -le 500 ]]; then
        echo "Failed to find suitable ID"
        exit 1
    fi
done

# Now create the group and user with the determined ID.
getent group coredns >/dev/null || groupadd -r -g $preferred_id coredns
getent passwd coredns >/dev/null || \
    useradd -r -u $preferred_id -g coredns -s /sbin/nologin \
    -c "CoreDNS services" -d /var/lib/coredns coredns


