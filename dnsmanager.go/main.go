package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"strings"

	"github.com/go-redis/redis/v8"
	"golang.org/x/net/context"
)

var (
	operation  string
	recordType string
	name       string
	value      string
)

func extractBaseDomain(recordName string) string {
	parts := strings.Split(recordName, ".")
	// fmt.Printf("[extractBaseDomain]parts: %s ): \n", parts)
	// fmt.Printf("[extractBaseDomain]return: %s \n", parts[len(parts)-2]+"."+parts[len(parts)-1]+".")
	return parts[len(parts)-2] + "." + parts[len(parts)-1] + "."
}

func addOrUpdateRecord(client *redis.Client, domain string, field string, rType string, rValue string) {
	ctx := context.Background()

	// Check if the zone exists
	if client.Exists(ctx, domain).Val() == 0 {
		fmt.Printf("Zone %s does not exist. Do you want to create it? (yes/no): ", domain)
		var response string
		fmt.Scanln(&response)
		if response != "yes" {
			fmt.Println("Operation aborted.")
			return
		}
	}

	var existingData map[string]interface{}
	existingJSON := client.HGet(ctx, domain, field).Val()

	if existingJSON != "" {
		if err := json.Unmarshal([]byte(existingJSON), &existingData); err != nil {
			fmt.Println("Error reading existing record:", err)
			return
		}
	}

	if existingData == nil {
		existingData = make(map[string]interface{})
	}

	var newData interface{}
	switch strings.ToLower(rType) {
	case "a": // OK
		newData = []map[string]interface{}{{
			"ip":  rValue,
			"ttl": 360,
		}}
	case "aaaa":
		newData = []map[string]interface{}{{
			"ip":  rValue,
			"ttl": 360,
		}}

	case "cname": // OK, but... I dont get back the IP with dig
		newData = []map[string]interface{}{{
			"host": rValue,
			"ttl":  360,
		}}
	case "txt": //  4) "{\"txt\":[{\"ttl\":300, \"text\":\"this is a wildcard\"}],\"mx\":[{\"ttl\":300, \"host\":\"host1.example.net.\",\"preference\": 10}]}" ////  8) "{\"txt\":[{\"ttl\":300, \"text\":\"this is not a wildcard\"}]}"
		newData = []map[string]interface{}{{
			"text": rValue,
			"ttl":  360,
		}}
	case "ns": // 12) "{\"ns\":[{\"ttl\":300, \"host\":\"ns1.subdel.example.net.\"},{\"ttl\":300, \"host\":\"ns2.subdel.example.net.\"}]}"
		newData = []map[string]interface{}{{
			"host": rValue,
			"ttl":  360,
		}}
	case "mx": //  4) "{\"txt\":[{\"ttl\":300, \"text\":\"this is a wildcard\"}],\"mx\":[{\"ttl\":300, \"host\":\"host1.example.net.\",\"preference\": 10}]}"
		newData = map[string]interface{}{
			"host":     rValue,
			"priority": 10,
			"ttl":      360,
		}
	case "srv": // 10) "{\"srv\":[{\"ttl\":300, \"target\":\"tcp.example.com.\",\"port\":123,\"priority\":10,\"weight\":100}]}"
		newData = map[string]interface{}{
			"host":     rValue,
			"port":     555,
			"priority": 10,
			"weight":   100,
			"ttl":      360,
		}
	if strings.ToLower(rType) == "soa" {
		field = "@*"
	}
	case "soa": // FUCKED 14) "{\"soa\":{\"ttl\":300, \"minttl\":100, \"mbox\":\"hostmaster.example.net.\",\"ns\":\"ns1.example.net.\",\"refresh\":44,\"retry\":55,\"expire\":66},\"ns\":[{\"ttl\":300, \"host\":\"ns1.example.net.\"},{\"ttl\":300, \"host\":\"ns2.example.net.\"}]}"
		newData = map[string]interface{}{
			"ttl":     100,
			"minttl":  100,
			"ns":      "ns1WhoamI.arpa.corp.",
			"refresh": 44,
			"retry":   55,
			"expire":  67,
		}
	case "caa": // FUCKED 16)"{\"caa\":[{\"flag\":0, \"tag\":\"issue\", \"value\":\"letsencrypt.org\"}]}"
		newData = map[string]interface{}{
			"flag":  0,
			"tag":   "issue",
			"value": rValue,
		}
		// ... [Other cases]
	}

	// Clear any existing record types
	for key := range existingData {
		delete(existingData, key)
	}

	existingData[strings.ToLower(rType)] = newData

	// Convert back to JSON and store in Redis
	dataJSON, err := json.Marshal(existingData)
	if err != nil {
		fmt.Println("Error preparing record data:", err)
		return
	}
	client.HSet(ctx, domain, field, dataJSON)

	fmt.Println("Record added or updated successfully!")
}

func main() {
	flag.StringVar(&operation, "op", "", "Operation to perform: add or remove")
	flag.StringVar(&recordType, "type", "", "DNS record type")
	flag.StringVar(&name, "name", "", "Record name")
	flag.StringVar(&value, "value", "", "Record value")

	flag.Parse()

	if operation == "" || recordType == "" || name == "" {
		fmt.Println("Please provide all required flags: operation (-op), type (-type), and name (-name).")
		return
	}

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "MyPasswoRdSucks",
		DB:       0,
	})

	// fmt.Printf("[main]name: %s ): \n", name)
	baseDomain := extractBaseDomain(name)
	domain := strings.TrimSuffix(baseDomain, ".")
	// fmt.Printf("[main]baseDomain: %s ): \n", baseDomain)
	field := strings.TrimSuffix(name, "."+domain)
	// fmt.Printf("[main]field: %s ): \n", field)


	switch operation {
	case "add":
		addOrUpdateRecord(client, baseDomain, field, recordType, value)
	// Add the remove functionality as needed
	default:
		fmt.Println("Unsupported operation. Only 'add' and 'remove' are supported.")
	}
}
