

```
 dnsmanager -op add -type CNAME -name my.fake.domain -value blue.grean.yellow.
 dnsmanager -op add -type A -name ns1.arpa.corp -value 10.0.0.100
 dnsmanager -op add -type A -name ns2.arpa.corp -value 10.0.0.101
 dnsmanager -op add -type A -name ns3.arpa.corp -value 10.0.0.102
 dnsmanager -op add -type CNAME -name ns.arpa.corp -value ns1.arpa.corp
 dnsmanager -op add -type SOA -name @.arpa.corp -value "IMHARDCODED"
 dnsmanager -op add -type a -name yd2.arpa.corp -value 10.2.3.2
 dnsmanager -op add -type CNAME -name yd.arpa.corp -value x.example.com
```


```

redis-cli -a MyPasswoRdSucks

127.0.0.1:6379> KEYS *
1) "fake.domain."

127.0.0.1:6379> HGETALL arpa.corp.
 1) "xdl"
 2) "{\"cname\":[{\"host\":\"x.example.com\",\"ttl\":360}]}"
 3) "arpa.corp."
 4) "{\"soa\":[{\"expire\":67,\"minttl\":100,\"ns\":\"ns1.arpa.corp.\",\"refresh\":44,\"retry\":55,\"ttl\":100}]}"
 5) "ns"
 6) "{\"cname\":[{\"host\":\"ns1.arpa.corp\",\"ttl\":360}]}"
 7) "ns3"
 8) "{\"a\":[{\"ip\":\"10.0.0.102\",\"ttl\":360}]}"
 9) "ns1"
10) "{\"a\":[{\"ip\":\"10.0.0.100\",\"ttl\":360}]}"
11) "yd2"
12) "{\"a\":[{\"ip\":\"10.2.3.2\",\"ttl\":360}]}"
13) "yd"
14) "{\"cname\":[{\"host\":\"x.example.com\",\"ttl\":360}]}"
15) "ns2"
16) "{\"a\":[{\"ip\":\"10.0.0.101\",\"ttl\":360}]}"
17) "@"
18) "{\"soa\":{\"expire\":67,\"minttl\":100,\"ns\":\"ns1WhoamI.arpa.corp.\",\"refresh\":44,\"retry\":55,\"ttl\":100}}"
19) "test"
20) "{\"a\":[{\"ip\":\"1.2.3.2\",\"ttl\":360}]}"
```


buuut

```
[root@CoreDNS03 ~ ] ❯ dig @10.0.0.100 ns.arpa.corp 

; <<>> DiG 9.18.17 <<>> @10.0.0.100 ns.arpa.corp
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9464
;; flags: qr aa rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: c359f99d84908d70 (echoed)
;; QUESTION SECTION:
;ns.arpa.corp.			IN	A

;; Query time: 2 msec
;; SERVER: 10.0.0.100#53(10.0.0.100) (UDP)
;; WHEN: Mon Oct 02 21:46:26 CEST 2023
;; MSG SIZE  rcvd: 53



[root@CoreDNS03 ~ ] ❯ dig @10.0.0.100 ns.arpa.corp CNAME

; <<>> DiG 9.18.17 <<>> @10.0.0.100 ns.arpa.corp CNAME
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 36687
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 6c687242aaaeb16d (echoed)
;; QUESTION SECTION:
;ns.arpa.corp.			IN	CNAME

;; ANSWER SECTION:
ns.arpa.corp.		300	IN	CNAME	ns1.arpa.corp.

;; Query time: 1 msec
;; SERVER: 10.0.0.100#53(10.0.0.100) (UDP)
;; WHEN: Mon Oct 02 22:18:08 CEST 2023
;; MSG SIZE  rcvd: 92
```


Its getting better!


```
[root@CoreDNS03 ~ ] took 5s ❯ dig +short SOA @10.0.0.100 arpa.corp 
;; Got bad packet: bad label type
112 bytes
84 38 85 00 00 01 00 01 00 00 00 01 04 61 72 70          .8...........arp
61 04 63 6f 72 70 00 00 06 00 01 04 61 72 70 61          a.corp......arpa
04 63 6f 72 70 00 00 06 00 01 00 00 00 64 00 29          .corp........d.)
09 6e 73 31 57 68 6f 61 6d 49 04 61 72 70 61 04          .ns1WhoamI.arpa.
63 6f 72 70 00 65 1b 1a 88 00 00 00 2c 00 00 00          corp.e......,...
37 00 00 00 43 00 00 00 64 00 00 29 04 d0 00 00          7...C...d..)....
00 00 00 0c 00 0a 00 08 e1 de cf 80 b8 97 8d d4          ................

```